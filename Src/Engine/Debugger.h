//
// Created by Anton on 9/28/2021.
//

#ifndef SIMMODEL_DEBUGGER_H
#define SIMMODEL_DEBUGGER_H

#include <iostream>
#include <map>

#include "Roadmap/RoadGraph.h"

namespace JATS::Engine {
    struct Debugger {
        static void print(const RoadGraph &graph);
    };
}

#endif //SIMMODEL_DEBUGGER_H

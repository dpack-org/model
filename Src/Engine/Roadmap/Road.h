//
// Created by Anton on 9/28/2021.
//

#ifndef SIMMODEL_ROAD_H
#define SIMMODEL_ROAD_H

#include "Private/GraphObj.h"

#include "Utils/ConstGetter.h"

namespace JATS::Engine {
    struct Waypoint;

    struct Road : public Private::GraphObj {
        // Immutable getters:
        Utils::ConstGetter <Waypoint> start;
        Utils::ConstGetter <Waypoint> dest;

        // Place for other props ( mutable )...

        ~Road() override = default;

        // Forwarded API ( forwarders to the corresponding RoadGraph' methods ):
        void destroy() const;

    protected:
        Road(RoadGraph &owner, Waypoint &start, Waypoint &dest);

        friend class RoadGraph;
    };
}

#endif //SIMMODEL_ROAD_H

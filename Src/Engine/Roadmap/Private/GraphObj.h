//
// Created by Anton on 9/28/2021.
//

#ifndef SIMMODEL_GRAPHOBJ_H
#define SIMMODEL_GRAPHOBJ_H

namespace JATS::Engine {
    struct RoadGraph;

    namespace Private {
        /**
         * \brief Virtual class for describing RoadGraph object
         * \details Has deleted copy-constructor/copy-assigment operator
         */
        struct GraphObj {
            GraphObj(RoadGraph &owner);
            GraphObj(const GraphObj&) = delete;

            virtual ~GraphObj() = default;

            RoadGraph &owner() const;

            // Operators:
            GraphObj &operator=(const GraphObj&) = delete;

        protected:
            RoadGraph &mOwner;
        };
    }
}

#endif //SIMMODEL_GRAPHOBJ_H

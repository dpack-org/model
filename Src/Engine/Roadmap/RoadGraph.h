//
// Created by Anton on 9/28/2021.
//

#ifndef SIMMODEL_ROADGRAPH_H
#define SIMMODEL_ROADGRAPH_H

#include "Waypoint.h"
#include "Road.h"

#include "Utils/Check.h"

#include "Utils/ArrayAccessor.h"
#include "Utils/DefaultConverters.h"

#include "Utils/ConstGetter.h"

#include <cassert>
#include <vector>

namespace JATS::Engine {
    class RoadGraph {
        template<typename DataT>
        using Accessor = Utils::ArrayAccessor<std::vector<DataT*>,
                                              const DataT&,
                                              Utils::Converters::Ptr2Ref<const DataT>>;

        template<typename DataT>
        using AccessorField = Utils::ConstGetter<Accessor<DataT>>;

        // Private typedefs:
        using RoadRefsInitializerList = const std::initializer_list<std::reference_wrapper<Road>> &;

        // Data:
        std::vector<Waypoint*> mWaypoints;
        std::vector<Road*> mRoads;

    public:
        RoadGraph(size_t tWaypointsNum = 0, size_t tRoadsNum = 0);
        ~RoadGraph();

        // Public API:
        Waypoint &addWaypoint();
        Road &addRoad(Waypoint &start, Waypoint &dest);

        void destroy(Road &road);

        // Getters:
        [[nodiscard]] const std::vector<Waypoint*> &rawWaypoints() const;
        [[nodiscard]] const std::vector<Road*> &rawRoads() const;

        const AccessorField<Waypoint> waypoints {this->mWaypoints};
        const AccessorField<Road> roads {this->mRoads};

        // TODO: waypointNum, roadsNum

    private:
        // Private API:
        // Check for object that belongs to the other graph
        inline void checkOwnerOfObject(const Private::GraphObj &obj);

    };
}

#endif //SIMMODEL_ROADGRAPH_H

//
// Created by Anton on 9/28/2021.
//

#ifndef SIMMODEL_WAYPOINT_H
#define SIMMODEL_WAYPOINT_H

#include "Private/GraphObj.h"
#include "Utils/ConstGetter.h"

#include <functional>
#include <vector>

namespace JATS::Engine {
    struct Road;

    struct Waypoint : public Private::GraphObj {
        using RoadRefsVec = std::vector<std::reference_wrapper<Road>>;

        // Immutable props:
        Utils::ConstGetter<RoadRefsVec> income {mIncome};
        Utils::ConstGetter<RoadRefsVec> outgoing {mOutgoing};

        // Place for mutable props ...

        ~Waypoint() override = default;

    protected:
        Waypoint(RoadGraph &owner, RoadRefsVec income, RoadRefsVec outgoing);

        RoadRefsVec mIncome;
        RoadRefsVec mOutgoing;

        friend class RoadGraph;
    };
}

#endif //SIMMODEL_WAYPOINT_H

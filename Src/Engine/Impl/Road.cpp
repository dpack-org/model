//
// Created by Anton on 9/28/2021.
//

#include "Engine/Roadmap/Road.h"
#include "Engine/Roadmap/Waypoint.h"

using namespace JATS::Engine;

Road::Road(RoadGraph &owner, Waypoint &start, Waypoint &dest) : Private::GraphObj(owner), start(start), dest(dest) {
    // nothing
}
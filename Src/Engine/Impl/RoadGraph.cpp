//
// Created by Anton on 9/28/2021.
//

#include "Engine/Roadmap/RoadGraph.h"

using namespace JATS::Engine;

// C-tor:
RoadGraph::RoadGraph(size_t tWaypointsNum, size_t tRoadsNum) {
    // Reserve memory in vectors ( performance optimization )
    this->mWaypoints.reserve(tWaypointsNum);
    this->mRoads.reserve(tRoadsNum);
}

RoadGraph::~RoadGraph() {
    // Delete everything
    for (const Waypoint* waypoint : this->mWaypoints) delete waypoint;
    for (const Road* road : this->mRoads) delete road;
}

// Public API:
Waypoint &RoadGraph::addWaypoint() {
    // Create waypoint
    Waypoint *waypoint = new Waypoint(*this, {}, {});

    this->mWaypoints.push_back(waypoint);
    return *waypoint;
}

Road &RoadGraph::addRoad(Waypoint &start, Waypoint &dest) {
    // Check for invalid owner
    this->checkOwnerOfObject(start);
    this->checkOwnerOfObject(dest);

    // Create road
    Road *road = new Road(*this, start, dest);
    this->mRoads.push_back(road); // allocate new road

    // Bind with waypoints:
    start.mOutgoing.emplace_back(*road);
    dest.mIncome.emplace_back(*road);

    return *road;
}

const std::vector<Waypoint *> &RoadGraph::rawWaypoints() const {
    return this->mWaypoints;
}

const std::vector<Road *> &RoadGraph::rawRoads() const {
    return this->mRoads;
}


// Private API:
void RoadGraph::checkOwnerOfObject(const Private::GraphObj &obj) {
   Utils::Check(this == &obj.owner(),
                "RoadGraph: Cannot work with object that doesn't belong to this graph");
}

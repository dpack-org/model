//
// Created by Anton on 9/28/2021.
//

#include "Engine/Roadmap/Waypoint.h"
#include "Engine/Roadmap/Road.h"

using namespace JATS::Engine;

Waypoint::Waypoint(RoadGraph &owner, RoadRefsVec income, RoadRefsVec outgoing)
     : Private::GraphObj(owner), mIncome(std::move(income)), mOutgoing(std::move(outgoing)) {

    // nothing
}


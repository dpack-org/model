//
// Created by Anton on 9/28/2021.
//

#include "Engine/Roadmap/Private/GraphObj.h"
#include "Engine/Roadmap/RoadGraph.h"

using namespace JATS::Engine;

Private::GraphObj::GraphObj(RoadGraph &owner) : mOwner(owner) { }

RoadGraph &Private::GraphObj::owner() const {
    return this->mOwner;
}


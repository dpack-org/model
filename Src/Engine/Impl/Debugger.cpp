//
// Created by Anton on 9/28/2021.
//

#include <Engine/Debugger.h>

void JATS::Engine::Debugger::print(const RoadGraph &graph)  {
    std::map<const Waypoint*, int> mapping;

    std::cout << "Vertex data:\n";

    // Create mapping
    int i = 0;
    for (const Waypoint* j : graph.rawWaypoints()){
        std::cout << i << "\n";
        mapping.insert(std::make_pair(j, i));

        ++i;
    }

    for (const Waypoint* j : graph.rawWaypoints()){
        const Waypoint::RoadRefsVec &outVertex = j->outgoing;
        int ID = mapping[j];

        for (const Road& r : outVertex){
            std::cout << ID << ' ' << mapping[&(r.dest())] << "\n";
        }
    }

    std::cout << "To visualize use:\nhttps://csacademy.com/app/graph_editor/\n";
}


//
// Created by Anton on 9/29/2021.
//

#ifndef SIMMODEL_ARRAYACCESSOR_H
#define SIMMODEL_ARRAYACCESSOR_H

#include "Private/ArrayAccessorBase.h"

namespace JATS::Utils {
    /**
     * \brief API for accessing array data. Used for seamless type casts
     *
     * @tparam ArrayT Type of the array
     * @tparam OutDataT Type of the returned data
     * @tparam ConverterT Type of the type converter ( optional )
     */
    template <typename ArrayT, typename OutDataT, typename ConverterT = void >
    struct ArrayAccessor;

    // Impl:
    template <typename ArrayT, typename OutDataT, typename ConverterT>
    class ArrayAccessor : public Private::ArrayAccessorBase<ArrayT, OutDataT, ConverterT> {

        // Private typedefs:
        typedef Private::ArrayAccessorBase<ArrayT, OutDataT, ConverterT> BaseT;

    public:
        // Public typedefs:
        typedef typename BaseT::Iterator Iterator;

        // C-tor
        ArrayAccessor(const ArrayT &array) : BaseT(array) { }

        // Public API:
        OutDataT at(size_t index) const override {
            return ConverterT::convert(this->mArray[index]);
        }
    };

    template <typename ArrayT, typename OutDataT>
    struct ArrayAccessor<ArrayT, OutDataT, void> : public Private::ArrayAccessorBase<ArrayT, OutDataT, void>{
        // Private typedefs:
        typedef Private::ArrayAccessorBase<ArrayT, OutDataT, void> BaseT;

    public:
        // C-tor
        ArrayAccessor(const ArrayT &array) : BaseT(array) { }

        // Public API:
        ArrayT at(size_t index) const override {
            return this->mArray[index];
        }
    };
}

#endif //SIMMODEL_ARRAYACCESSOR_H

//
// Created by Anton on 9/29/2021.
//

#ifndef SIMMODEL_DEFAULTCONVERTERS_H
#define SIMMODEL_DEFAULTCONVERTERS_H

#include <type_traits>

namespace JATS::Utils::Converters {
    /**
     * \brief Convert T* to T&
     * @tparam T Any cpp typename
     */
    template <typename T>
    struct Ptr2Ref {
        static_assert(!std::is_pointer_v<T> && !std::is_reference_v<T>, "T cannot be a pointer or a ref");

        static inline T& convert(T* value){
            return *value;
        }
    };
}

#endif //SIMMODEL_DEFAULTCONVERTERS_H

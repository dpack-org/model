//
// Created by Anton on 9/29/2021.
//

#ifndef SIMMODEL_ARRAYACCESSORBASE_H
#define SIMMODEL_ARRAYACCESSORBASE_H

namespace JATS::Utils::Private {
    template <typename ArrayT, typename OutDataT, typename ConverterT>
    struct ArrayAccessorBase {
        // Typedefs:
        typedef ArrayAccessorBase<ArrayT, OutDataT, ConverterT> SelfT;

        // Public classes:
        class Iterator {
            size_t mIndex = 0;
            const SelfT* mOwner;

        public:
            inline OutDataT operator()() const {
                return mOwner->at(this->mIndex);
            }
            OutDataT operator*() const {
                return this->operator()();
            }

            bool operator!=(const Iterator &other) const {
                return this->mIndex != other.mIndex;
            }

            void operator++(){
                ++mIndex;
            }

        protected:
            Iterator(SelfT* owner, size_t index) : mOwner(owner), mIndex(index) { }

            friend SelfT;
        };

        // C-tor/D-tor:
        ArrayAccessorBase(const ArrayT &array) : mArray(array) { }
        virtual ~ArrayAccessorBase() = default;

        // Virtual API:
        virtual OutDataT at(size_t index) const = 0;

        // Operators
        Iterator begin() const {
            return Iterator((SelfT*)this, 0);
        }
        Iterator end() const {
            return Iterator((SelfT*)this, this->mArray.size());
        }

    protected:
        // Props:
        const ArrayT &mArray;
    };
}

#endif //SIMMODEL_ARRAYACCESSORBASE_H

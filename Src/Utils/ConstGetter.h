//
// Created by Anton on 9/29/2021.
//

#ifndef SIMMODEL_CONSTGETTER_H
#define SIMMODEL_CONSTGETTER_H

#include <type_traits>

namespace JATS::Utils {
    /**
     * \brief Getter that provides access to an immutable value ( method/function call operator )
     * @tparam T Type of the held data
     */
    template <typename T>
    struct ConstGetter {
        // Public typedefs:
        typedef std::remove_reference_t<std::remove_const_t<T>> ValueT;

    private:
        const T &mValue;

    public:
        // C-tor:
        ConstGetter(const T &value);

        // Public API:
        inline const T& value() const;

        // Operators:
        inline const T& operator()() const;
        inline operator const T&() const;
    };

    template <typename T>
    using _ = const ConstGetter<T>;
} // namespace JATS::Utils

#include "Utils/Impl/ConstGetter.cpp"

#endif //SIMMODEL_CONSTGETTER_H

//
// Created by Anton on 9/28/2021.
//

#ifndef SIMMODEL_CHECK_H
#define SIMMODEL_CHECK_H

#include <iostream>


namespace JATS::Utils {
    /**
     * \brief Check if expr is true, otherwise interrupt program
     *
     * @param expr Checked expression
     * @param errorMsg Error message ( in case of expr failed )
     * @param exitStatus Exit status with which program will exit ( in case of expr failed )
     */
    inline bool Check(bool expr, const char* errorMsg, int exitStatus = -1){
        if (!expr){
            std::cerr << "(Check failed) " << errorMsg << std::endl;
            exit(exitStatus);
        }

        return true;
    }
}

#endif //SIMMODEL_CHECK_H

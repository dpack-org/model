//
// Created by Anton on 9/20/2021.
//

#ifndef SIMMODEL_USINGS_H
#define SIMMODEL_USINGS_H

namespace JATS {
    using size_t = unsigned long long;
    using uint_t = unsigned int;
    using byte_t = unsigned char;

    using time_t = size_t;

    struct SFINAE_Yes { char c; };
    struct SFINAE_No { char c[2]; };
}

#endif //SIMMODEL_USINGS_H

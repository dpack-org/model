//
// Created by Anton on 9/29/2021.
//

#ifndef SIMMODEL_CONSTGETTER_CPP
#define SIMMODEL_CONSTGETTER_CPP

#include <Utils/ConstGetter.h>

template<typename T>
JATS::Utils::ConstGetter<T>::ConstGetter(const T &value) : mValue(value) { }

// Public API:
template<typename T>
const T &JATS::Utils::ConstGetter<T>::value() const {
    return this->mValue;
}

// Operators:
template<typename T>
const T &JATS::Utils::ConstGetter<T>::operator()() const {
    return this->mValue;
}

template<typename T>
JATS::Utils::ConstGetter<T>::operator const T &() const {
    return this->mValue;
}

#endif //SIMMODEL_CONSTGETTER_CPP
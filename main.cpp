#include <Engine/Debugger.h>
#include <Engine/Roadmap/RoadGraph.h>
#include <Utils/ArrayAccessor.h>

using namespace JATS;

int main() {
    Engine::RoadGraph testGraph;
    Engine::RoadGraph testGraph2;

    // testGraph.road(id).split().;
    // Waypoint &j = testGraph.addWaypoint();

    Engine::Waypoint &j = testGraph.addWaypoint();
    Engine::Waypoint &j2 = testGraph.addWaypoint();

    Engine::Road &r = testGraph.addRoad(j, j);
    Engine::Road &r2 = testGraph.addRoad(j, j2);
    Engine::Road &r3 = testGraph.addRoad(j2, j);



    Engine::Debugger::print(testGraph);

    return 0;
}

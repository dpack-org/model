cmake_minimum_required(VERSION 3.20)
project(SimModel)

set(CMAKE_CXX_STANDARD 17)

include_directories(Src/)

add_executable(SimModel main.cpp
        Src/Utils/Usings.h Src/Utils/Check.h Src/Utils/ArrayAccessor.h
        #Src/Utils/Callback.h

        # Roadmap graph system
        Src/Engine/Roadmap/Roadmap.h Src/Engine/Roadmap/RoadGraph.h

        # Rest of the simulation engine
        Src/Engine/Debugger.h Src/Engine/Impl/Debugger.cpp
        Src/Engine/Roadmap/Waypoint.h Src/Engine/Roadmap/Private/GraphObj.h
        Src/Engine/Roadmap/Road.h

        Src/Engine/Impl/Debugger.cpp Src/Engine/Impl/Road.cpp
        Src/Engine/Impl/Waypoint.cpp Src/Engine/Impl/GraphObj.cpp Src/Engine/Impl/RoadGraph.cpp

        Src/Utils/Private/ArrayAccessorBase.h Src/Utils/DefaultConverters.h Src/Utils/ConstGetter.h Src/Utils/Impl/ConstGetter.cpp Src/Utils/ConstGetter.h)
